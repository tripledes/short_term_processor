package org.mdas.stprocessor.controllers;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.sse.SseEventSource;

import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;
import org.mdas.stprocessor.streams.FakeKafkaResource;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;

/**
 * Testing the SSE endpoint using FakeKafkaResource (container)
 */
@QuarkusTest
@QuarkusTestResource(FakeKafkaResource.class)
public class MeasureServerSideEventsTest {

    private static final String MEASURES_SSE_ENDPOINT = "http://localhost:8081/watch/stream";
    private static final Logger LOG = Logger.getLogger(MeasureServerSideEventsTest.class);

    @Test
    public void testMeasuresEventStream() throws InterruptedException {
        List<String> received = new CopyOnWriteArrayList<>();
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(MEASURES_SSE_ENDPOINT);
        try (SseEventSource source = SseEventSource.target(target).build()) {
            source.register((inboundSseEvent) -> received.add(inboundSseEvent.readData()));
            source.open();
            await().atMost(100000, MILLISECONDS).until(() -> received.size() == 1);
            LOG.info("successfully received SSE!");
            source.close();
        }
    }
}
