package org.mdas.stprocessor.controllers;

import java.net.URI;
import java.util.concurrent.CountDownLatch;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.WebSocketContainer;

import org.jboss.logging.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mdas.stprocessor.streams.FakeKafkaResource;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;

/**
 * Testing the websocket endpoint using FakeKafkaResource (container)
 */
@QuarkusTest
@QuarkusTestResource(FakeKafkaResource.class)
public class MeasureWebSocketTest {

    private static final String MEASURE_WEBSOCKET_ENDPOINT = "ws://localhost:8081/websocket";
    private static final Logger LOG = Logger.getLogger(MeasureWebSocketTest.class);
    private URI uri = URI.create(MEASURE_WEBSOCKET_ENDPOINT);

    @Test
    public void testWebsocketShouldReceivedMsgs() throws Exception {
        Client client = new Client();
        WebSocketContainer wsc = ContainerProvider.getWebSocketContainer();
        wsc.connectToServer(client, uri);
        Assertions.assertEquals(0L, client.getCaseCount());
    }

    @ClientEndpoint
    public static class Client {
        private final CountDownLatch latch = new CountDownLatch(1);

        public long getCaseCount() throws InterruptedException {
            latch.await();
            return latch.getCount();
        }

        @OnMessage
        public void onMessage(String msg) {
            LOG.info("successfully got data on the WS!");
            latch.countDown();
        }

        @OnError
        public void onError(Throwable t) {
            latch.countDown();
            t.printStackTrace();
        }
    }

}
