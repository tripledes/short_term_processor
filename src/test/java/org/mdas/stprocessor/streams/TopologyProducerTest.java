package org.mdas.stprocessor.streams;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;
import io.quarkus.kafka.client.serialization.JsonbSerializer;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.TestRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mdas.stprocessor.model.Aggregation;
import org.mdas.stprocessor.model.Measure;
import org.mdas.stprocessor.model.MeasureObjectMother;
import org.mdas.stprocessor.model.Sensor;
import org.mdas.stprocessor.model.SensorObjectMother;

import javax.inject.Inject;
import java.util.Properties;

import static org.mdas.stprocessor.streams.TopologyProducer.ST_TOPIC;
import static org.mdas.stprocessor.streams.TopologyProducer.MEASURE_TOPIC;
import static org.mdas.stprocessor.streams.TopologyProducer.SENSOR_STORE;
import static org.mdas.stprocessor.streams.TopologyProducer.SENSOR_TOPIC;

/**
 * Testing of the Topology without a broker, using TopologyTestDriver
 */
@QuarkusTest
public class TopologyProducerTest {

    @Inject
    Topology topology;

    TopologyTestDriver testDriver;

    TestInputTopic<String, Measure> temperatures;

    TestInputTopic<String, Sensor> sensors;

    TestOutputTopic<String, Aggregation> temperaturesAggregated;

    @BeforeEach
    public void setUp() {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "testApplicationId");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        testDriver = new TopologyTestDriver(topology, config);

        temperatures = testDriver.createInputTopic(MEASURE_TOPIC, new StringSerializer(), new JsonbSerializer());
        sensors = testDriver.createInputTopic(SENSOR_TOPIC, new StringSerializer(), new JsonbSerializer());

        temperaturesAggregated = testDriver.createOutputTopic(ST_TOPIC, new StringDeserializer(),
                new JsonbDeserializer<>(Aggregation.class));
    }

    @AfterEach
    public void tearDown() {
        testDriver.getTimestampedKeyValueStore(SENSOR_STORE).flush();
        testDriver.close();
    }

    @Test
    public void test() {
        Sensor sensor = SensorObjectMother.montcada();
        Measure measure = MeasureObjectMother.measureFromMontcada();
        sensors.pipeInput(sensor.getId(), sensor);
        temperatures.pipeInput(sensor.getId(), measure);
        temperatures.pipeInput(sensor.getId(), measure);

        temperaturesAggregated.readRecord();
        TestRecord<String, Aggregation> result = temperaturesAggregated.readRecord();

        Assertions.assertEquals("S1", result.getValue().id);
        Assertions.assertEquals("Montcada", result.getValue().metadata.getArea());
        Assertions.assertEquals(29.5f, result.getValue().current);
    }
}
