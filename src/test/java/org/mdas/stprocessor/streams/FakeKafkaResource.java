package org.mdas.stprocessor.streams;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.mdas.stprocessor.model.Measure;
import org.mdas.stprocessor.model.MeasureObjectMother;
import org.mdas.stprocessor.model.Sensor;
import org.testcontainers.containers.KafkaContainer;

import io.quarkus.kafka.client.serialization.ObjectMapperSerializer;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

/*
 * FakeKafkaResource manages the lifecycle of a kafka container for testing the application
 */
public class FakeKafkaResource implements QuarkusTestResourceLifecycleManager {

    private final KafkaContainer kafka = new KafkaContainer("5.4.2");

    public static Producer<String, Sensor> createSensorProducer(KafkaContainer kafka) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers());
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "test-sensor-st-processor");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ObjectMapperSerializer.class.getName());

        return new KafkaProducer<String, Sensor>(props);
    }

    public static Producer<String, Measure> createMeasureProducer(KafkaContainer kafka) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers());
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "test-measure-st-processor");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ObjectMapperSerializer.class.getName());

        return new KafkaProducer<String, Measure>(props);
    }

    @Override
    public Map<String, String> start() {
        kafka.start();
        Map<String, String> map = new HashMap<>();
        map.put("%test.kafka.bootstrap.servers", kafka.getBootstrapServers());
        map.put("%test.quarkus.kafka-streams.bootstrap-servers", kafka.getBootstrapServers());
        Producer<String, Sensor> sensorProducer = createSensorProducer(kafka);
        Producer<String, Measure> measureProducer = createMeasureProducer(kafka);
        Measure measure = MeasureObjectMother.measureFromMontcada();
        sensorProducer.send(new ProducerRecord<String, Sensor>("sensor-topic", measure.getMetadata().getId(),
                measure.getMetadata()));
        measureProducer
                .send(new ProducerRecord<String, Measure>("measure-topic", measure.getMetadata().getId(), measure));
        measureProducer
                .send(new ProducerRecord<String, Measure>("measure-topic", measure.getMetadata().getId(), measure));
        measureProducer
                .send(new ProducerRecord<String, Measure>("measure-topic", measure.getMetadata().getId(), measure));
        return map;
    }

    @Override
    public void stop() {
        kafka.close();
    }
}
