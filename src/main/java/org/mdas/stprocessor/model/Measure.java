package org.mdas.stprocessor.model;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

/**
 * The actual measure, embedding metadata
 */
public class Measure {
    private final Float temperature;
    private final String takenAt;
    private final Sensor metadata;

    @JsonbCreator
    public Measure(@JsonbProperty("temperature") Float temperature, @JsonbProperty("takenAt") String takenAt,
            @JsonbProperty("metadata") Sensor metadata) {
        this.temperature = temperature;
        this.takenAt = takenAt;
        this.metadata = metadata;
    }

    public Float getTemperature() {
        return temperature;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public Sensor getMetadata() {
        return metadata;
    }
}