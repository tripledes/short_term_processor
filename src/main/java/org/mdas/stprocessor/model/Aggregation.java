
package org.mdas.stprocessor.model;

import javax.validation.constraints.NotNull;

/**
 * Aggregation class helps creating the materialized view of the temperatures
 * being sent by the network of sensors.
 */
public class Aggregation {
    public Sensor metadata;
    public double current = 0.0;
    public int count = 0;
    public String takenAt;
    public String id;

    /**
     * updateFrom extracts data and builds the updated measure for a given sensor Id
     * 
     * @param measure A Measure object, should come from the measures topic,
     *                deserialized from JSON.
     * @return The current temperature, measure count and sensor metadata.
     */
    public Aggregation updateFrom(@NotNull Measure measure) {
        this.metadata = measure.getMetadata();
        this.id = this.metadata.getId();
        this.current = measure.getTemperature();
        this.count++;
        this.takenAt = measure.getTakenAt();
        return this;
    }
}
