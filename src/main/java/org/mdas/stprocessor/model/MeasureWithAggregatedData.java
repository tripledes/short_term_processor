package org.mdas.stprocessor.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class MeasureWithAggregatedData {

    public Sensor sensor;
    public int count;
    public double current;
    public String takenAt;
    public String id;

    private MeasureWithAggregatedData(String id, Sensor sensor, int count, double current,
                                      String takenAt) {
        this.sensor = sensor;
        this.count = count;
        this.current = current;
        this.takenAt = takenAt;
        this.id = id;
    }

    public static MeasureWithAggregatedData from(Aggregation aggregation) {
        return new MeasureWithAggregatedData(
                aggregation.metadata.getId(),
                aggregation.metadata,
                aggregation.count,
                aggregation.current,
                aggregation.takenAt);
    }
}
