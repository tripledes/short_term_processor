package org.mdas.stprocessor.controllers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.Session;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.jboss.logging.Logger;

/**
 * MeasuresWebSocketEndpoint creates a websocket service to push measures from
 * the short-term-streams sink.
 */
@ServerEndpoint("/websocket")
@ApplicationScoped
public class MeasuresWebSocketEndpoint {

    private static final Logger LOG = Logger.getLogger(MeasuresWebSocketEndpoint.class);
    Map<String, Session> sessions = new ConcurrentHashMap<>();

    /**
     * onOpen - registers new sessions
     * 
     * @param session - new session
     */
    @OnOpen
    public void onOpen(Session session) {
        sessions.put(session.getId(), session);
        LOG.infov("Session ID {0} connected", session.getId());
    }

    /**
     * onClose - removes closed sessions
     * 
     * @param session - disconnected session
     */
    @OnClose
    public void onClose(Session session) {
        sessions.remove(session.getId());
        LOG.infov("Session ID {0} disconnected", session.getId());
    }

    /**
     * onError - removes sessions on errors and logs them
     * 
     * @param session   - session with errors
     * @param throwable - error itself
     */
    @OnError
    public void onError(Session session, Throwable throwable) {
        sessions.remove(session.getId());
        LOG.errorv("Session ID {0} disconnected with error: {1}", session.getId(), throwable);
    }

    /**
     * stream - gets messages from the short-term-streams sink and pushes them to
     * all registered sessions
     * 
     * @param message - message resulting from the streams pipeline, stringyfied
     */
    @Incoming("websocket-channel")
    public void stream(String message) {
        sessions.values().forEach(s -> {
            s.getAsyncRemote().sendObject(message, result -> {
                if (result.getException() != null) {
                    LOG.errorv("Unable to send message: {0}", result.getException());
                }
            });
        });
    }
}
