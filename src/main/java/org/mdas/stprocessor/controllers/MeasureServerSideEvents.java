package org.mdas.stprocessor.controllers;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.SseElementType;
import org.mdas.stprocessor.model.Aggregation;
import org.reactivestreams.Publisher;

import org.eclipse.microprofile.reactive.messaging.Channel;

/**
 * Resource to emit SSEs coming from the sse-channel
 */
@ApplicationScoped
@Path("/watch")
public class MeasureServerSideEvents {

    @Inject
    @Channel("sse-channel")
    Publisher<Aggregation> aggregation;

    @GET
    @Path("/stream")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType("application/json")
    public Publisher<Aggregation> stream() {
        return aggregation;
    }
}
